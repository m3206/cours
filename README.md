## Ce dépot contient les cours de m3206 

### L'objectif

L'objectif de ce cours et de vous faire comprendre l'intérêt de la ligne de commande et vous rendre plus à l'aise et plus efficace devant votre ligne de commande.
Le problème de ce genre d'objectif c'est que si vous n'avez pas envie, c'est une cause perdue d'avance pour moi.
 
Vous vous demandez surement pourquoi, l'objectif annoncé ici est différent du nom du cours qui est "automatisation des tâches d'administration". La raison est assez simple.
L'automatisation des tâches d'administration elle même est assez simple à mettre en oeuvre une fois que à êtes à l'aise devant votre ligne de commande. 
Donc dans ce cours, on ne brule pas les étapes, et on commence par vous mettre à l'aise devant votre ligne de commande.


### Déroulement "C'est en committant qu'on devient developpeur". 

Durant tout ce cours, vous devrez être devant votre ligne de commande. Pour cela, il vous faut une machine virtuelle 
qui ne contient qu'une ligne de commande rien de plus. __Pas d'affichage graphique__. 
Choissisez un système d'exploitation Unix ou Linux c'est le moment d'essayer un système que vous ne connaissez pas.

Tous votre travail doit se trouver dans un dépôt git.



### Ressources
- Un pdf du [cours](https://gitlab.com/m3206/cours/blob/master/files/m3206.pdf)
- une introduction à git en [vidéo](https://gitlab.com/trazafin/git) et un [pdf](https://gitlab.com/m3206/cours/blob/master/files/m3206-git.pdf)
- Une introduction à [vim](https://gitlab.com/m3206/cours/blob/master/files/m3206-vim.pdf)
- La survie [find, grep, sed, awk](https://gitlab.com/m3206/cours/blob/master/files/m3206-find-grep.pdf)



## Remarques 
Si vous avez des remarques (fautes, questions, découvertes, lien intéressants, ... peu importe en fait tant
que c'est lié plus ou moins ou cours) 
concernant le cours, ouvrez une issue. Cet onglet ce trouve en haut de cette page.

Vous avez la listes des onglets: `Project/Activity/.../Issues`


[une minute de pause](http://www.commitstrip.com/en/2016/11/23/if-coders-were-footballers-2?)

http://explainshell.com
### Eléments d'un script shell

#### Un nouveau mode d’utilisation du shell: mode interactif, mode script

L'exemple "Hello World !!!"

```bash
#!/bin/bash
 echo  "Hello" 
 echo  "world !!!"
```

#### Utilisation des variables

Exemple de manipulation de variables

```bash
#!/bin/bash
 AGE=45
 NOM=Henri
 echo  "Monsieur $NOM a $AGE ans"
```

#### Exploiter les arguments d’un script

Arguments par position
```bash
#!/bin/bash
 echo  "Il y a $# argument(s)" 
 echo  "Le 1er  est : $1"
 ```
 
```bash 
commande  a  b  c


$1 → a
$2 → b
$3 → c
$#  → 3   (nombre d’arguments existants)
$@ → { a b c }   (tous les arguments dans une liste)
```

Exemple:
```bash
#!/bin/bash
echo  "Il y a $# argument(s)"
echo  "Le 1er  est : $1" 
```


#### Syntaxes avancées des variables pour mieux gérer les arguments et variables
[variables expansions](https://tiswww.case.edu/php/chet/bash/bashref.html#Shell-Parameter-Expansion)

`${nom:-valeur}`:  renvoie la valeur de la variable de nom `nom` si elle en contient une, ou `valeur` sinon. Permet d’utiliser une valeur par défaut

`${nom:?mess}`: renvoie la valeur de la variable de nom `nom` si elle en contient une, ou sinon affiche le message d’erreur `mess` et quitte le script. Permet de contrôler l’existence d’argument

```bash
#!/bin/bash

echo  "Il y a $# argument(s)"

FIRST=${1:-vide} 
echo  "Le 1er  est : $FIRST" 
```

```bash
#!/bin/bash

ARG1=${1:?"Vous devez fournir un argument"}

echo  "Le 1er  est : $ARG1" 
```

### Les fonctions

#### Syntaxe
```bash
nom_fonction() {  
	commande1
	commande2
	 …
    }
```

Exemple
```bash
#!/bin/bash

#définition de la fonction
say_hello() {
	echo  "Hello World !!!" 
}

#utilisation de la fonction
say_hello
say_hello
```

#### Paramètres de fonction

```bash
nom_fonction   arg1  arg2  …  argN
```

Dans le code de la fonction : On récupère les paramètres avec la même syntaxe que les arguments du script (`$1, $2, $#, $@, ...`)

Exemple
```bash
#!/bin/bash

#définition de la fonction
say_hello() {
	echo  "Hello $1 !!!" 
}

#utilisation de la fonction
say_hello Me
say_hello You
```

#### Variables locales

Les variables d’un script sont globales au fichier. Même les variables manipulées dans le code d’une fonction. Pour définir une variable locale au code d’une fonction :
`local nom_variable` ou `local nom_variable=valeur`

#### Valeurs de retour d'une fonction

__La valeur de retour est un code numérique (0 à 255)__


Cette valeur est soit : celle de la dernière commande exécutée par la fonction, celle en argument de la commande : `return val` utilisée pour mettre fin à l’exécution de la fonction

Dans le code qui utilise la fonction, on récupère la valeur de retour dans la variable $? (juste après l’appel)

Exemple

```bash
#!/bin/bash

max() {
	if [ $1 –gt  $2 ]; then
		return $1
	else 
		return $2
	fi 
}

IS_OK=${2:?"Vous devez fournir 2 valeurs"}
max $1 $2
echo $?
```

Ou mieux si `return`renvoi une valeur supérieur à 255

```bash
#!/bin/bash

max() {
	if [ $1 –gt  $2 ]; then
		echo $1
	else 
		echo $2
	fi 
}

IS_OK=${2:?"Vous devez fournir 2 valeurs"}
REP=$(max $1 $2)
echo $REP
```